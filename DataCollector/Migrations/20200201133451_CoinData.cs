﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DataCollector.Migrations
{
    public partial class CoinData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "BookTicker",
                columns: table => new
                {
                    BookTickerId = table.Column<long>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    ExchangeId = table.Column<long>(nullable: false),
                    CoinId = table.Column<long>(nullable: false),
                    BidPrice = table.Column<decimal>(nullable: false),
                    BidQty = table.Column<long>(nullable: false),
                    AskPrice = table.Column<decimal>(nullable: false),
                    AskQty = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BookTicker", x => x.BookTickerId);
                    table.ForeignKey(
                        name: "FK_BookTicker_Coins_CoinId",
                        column: x => x.CoinId,
                        principalTable: "Coins",
                        principalColumn: "CoinId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BookTicker_Exchanges_ExchangeId",
                        column: x => x.ExchangeId,
                        principalTable: "Exchanges",
                        principalColumn: "ExchangeId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "DayTicker",
                columns: table => new
                {
                    DayTickerId = table.Column<long>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    ExchangeId = table.Column<long>(nullable: false),
                    CoinId = table.Column<long>(nullable: false),
                    PriceChange = table.Column<decimal>(nullable: false),
                    PriceChangePercent = table.Column<decimal>(nullable: false),
                    PrevClosePrice = table.Column<decimal>(nullable: false),
                    LastPrice = table.Column<decimal>(nullable: false),
                    LastQty = table.Column<decimal>(nullable: false),
                    BidPrice = table.Column<decimal>(nullable: false),
                    AskPrice = table.Column<decimal>(nullable: false),
                    OpenPrice = table.Column<decimal>(nullable: false),
                    HighPrice = table.Column<decimal>(nullable: false),
                    LowPrice = table.Column<decimal>(nullable: false),
                    Volume = table.Column<decimal>(nullable: false),
                    OpenTime = table.Column<DateTime>(nullable: false),
                    CloseTime = table.Column<DateTime>(nullable: false),
                    Count = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DayTicker", x => x.DayTickerId);
                    table.ForeignKey(
                        name: "FK_DayTicker_Coins_CoinId",
                        column: x => x.CoinId,
                        principalTable: "Coins",
                        principalColumn: "CoinId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DayTicker_Exchanges_ExchangeId",
                        column: x => x.ExchangeId,
                        principalTable: "Exchanges",
                        principalColumn: "ExchangeId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Klines",
                columns: table => new
                {
                    KlineId = table.Column<long>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    ExchangeId = table.Column<long>(nullable: false),
                    CoinId = table.Column<long>(nullable: false),
                    OpenTime = table.Column<DateTime>(nullable: false),
                    Open = table.Column<decimal>(nullable: false),
                    High = table.Column<decimal>(nullable: false),
                    Low = table.Column<decimal>(nullable: false),
                    Close = table.Column<decimal>(nullable: false),
                    Volume = table.Column<decimal>(nullable: false),
                    NumberOfTrades = table.Column<long>(nullable: false),
                    CloseTime = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Klines", x => x.KlineId);
                    table.ForeignKey(
                        name: "FK_Klines_Coins_CoinId",
                        column: x => x.CoinId,
                        principalTable: "Coins",
                        principalColumn: "CoinId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Klines_Exchanges_ExchangeId",
                        column: x => x.ExchangeId,
                        principalTable: "Exchanges",
                        principalColumn: "ExchangeId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_BookTicker_CoinId",
                table: "BookTicker",
                column: "CoinId");

            migrationBuilder.CreateIndex(
                name: "IX_BookTicker_ExchangeId",
                table: "BookTicker",
                column: "ExchangeId");

            migrationBuilder.CreateIndex(
                name: "IX_DayTicker_CoinId",
                table: "DayTicker",
                column: "CoinId");

            migrationBuilder.CreateIndex(
                name: "IX_DayTicker_ExchangeId",
                table: "DayTicker",
                column: "ExchangeId");

            migrationBuilder.CreateIndex(
                name: "IX_Klines_CoinId",
                table: "Klines",
                column: "CoinId");

            migrationBuilder.CreateIndex(
                name: "IX_Klines_ExchangeId",
                table: "Klines",
                column: "ExchangeId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BookTicker");

            migrationBuilder.DropTable(
                name: "DayTicker");

            migrationBuilder.DropTable(
                name: "Klines");
        }
    }
}
