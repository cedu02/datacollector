﻿using DataCollector.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataCollector.Core.Models {
    public class CoinData {
        public ExchangeModel Exchange { get; set; } = new ExchangeModel();
        public CoinModel Coin { get; set; } = new CoinModel();
        public BookTickerModel BookTicker { get; set; } = new BookTickerModel();
        public DayTickerModel DayTicker { get; set; } = new DayTickerModel();
        public List<KlineModel> Klines { get; set; } = new List<KlineModel>();
    }
}
