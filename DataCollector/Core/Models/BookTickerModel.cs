﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataCollector.Core.Models {
    public class BookTickerModel {
        public decimal BidPrice { get; set; }
        public long BidQty { get; set; }
        public decimal AskPrice { get; set; }
        public long AskQty { get; set; }
    }
}
