﻿using System;

namespace DataCollector.Models {
    public class DayTickerModel {
        public Decimal PriceChange { get; set; }
        public Decimal PriceChangePercent { get; set; }
        public Decimal PrevClosePrice { get; set; }
        public Decimal LastPrice { get; set; }
        public Decimal LastQty { get; set; }
        public Decimal BidPrice { get; set; }
        public Decimal AskPrice { get; set; }
        public Decimal OpenPrice { get; set; }
        public Decimal HighPrice { get; set; }
        public Decimal LowPrice { get; set; }
        public Decimal Volume { get; set; }
        public DateTime OpenTime { get; set; }
        public DateTime CloseTime { get; set; }
        public long Count { get; set; }
    }
}
