﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataCollector.Core.Models {
    public class ExchangeModel {
        public long ExchangeId { get; set; }
        public string Name { get; set; } = string.Empty;
        public string BaseApiUrl { get; set; } = string.Empty;
    }
}
