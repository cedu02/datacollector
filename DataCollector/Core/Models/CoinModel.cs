﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataCollector.Core.Models {
    public class CoinModel {
        public long CoinId { get; set; }
        public string Name { get; set; } = string.Empty;
    }
}
