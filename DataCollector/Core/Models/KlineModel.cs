﻿using System;

namespace DataCollector.Models {
    public class KlineModel {

        public DateTime OpenTime { get; set; }
        public decimal Open { get; set; }
        public decimal High { get; set; }
        public decimal Low { get; set; }
        public decimal Close { get; set; }
        public decimal Volume { get; set; }
        public long NumberOfTrades { get; set; }
        public DateTime CloseTime { get; set; }

    }
}
