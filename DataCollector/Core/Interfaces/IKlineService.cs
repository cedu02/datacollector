﻿using DataCollector.Models;
using System.Threading.Tasks;

namespace DataCollector.Interfaces {
    /// <summary>
    /// KlineService Interface contract for 
    /// each Exchange there has to be a implementation.
    /// </summary>
    public interface IKlineService {
        /// <summary>
        /// Should return the newest Kline form a Source/Api 
        /// for a given Endpoint.
        /// </summary>
        /// <param name="endpoint"></param>
        /// <returns>Kline</returns>
        public Task<KlineModel> GetKline(IEndpoint endpoint);
    }
}
