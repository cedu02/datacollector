﻿using DataCollector.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DataCollector.Core.Interfaces {
    public interface ICoinDataSaveService {
        public Task<bool> SaveData(CoinData data); 
    }
}
