﻿using DataCollector.Core.Models;
using System.Threading.Tasks;

namespace DataCollector.Interfaces {
    public interface IBookTickerService {
        public Task<BookTickerModel> GetBookTicker(IEndpoint endpoint);
    }
}
