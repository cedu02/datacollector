﻿using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace DataCollector.Interfaces {
    public interface IHttpGetService {

        public Task<HttpResponseMessage> SendGetRequest(String endpoint);

    }
}
