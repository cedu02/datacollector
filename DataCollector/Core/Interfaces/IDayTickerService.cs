﻿using DataCollector.Models;
using System.Threading.Tasks;

namespace DataCollector.Interfaces {
    public interface IDayTickerService {

        public Task<DayTickerModel> GetDayTicker(IEndpoint endpoint);
    }
}
