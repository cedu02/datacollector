﻿using DataCollector.Core.Models;
using DataCollector.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DataCollector.Core.Interfaces.Repositories {
    public interface IDayTickerRepository {
        public Task<bool> SaveDayTicker(
            CoinModel coin,
            ExchangeModel exchange, 
            DayTickerModel dayTicker);
    }
}
