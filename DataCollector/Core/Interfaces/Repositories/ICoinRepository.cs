﻿using DataCollector.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DataCollector.Core.Interfaces.Repositories {
    public interface ICoinRepository {
        public Task<List<CoinModel>> GetActiveCoins();
    }
}
