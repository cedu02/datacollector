﻿using DataCollector.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DataCollector.Core.Interfaces.Repositories {
    public interface IExchangeRepository {
        public Task<List<ExchangeModel>> GetExchangesOfCoin(CoinModel coin);
    }
}
