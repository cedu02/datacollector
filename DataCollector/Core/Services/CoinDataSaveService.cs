﻿using DataCollector.Core.Interfaces;
using DataCollector.Core.Interfaces.Repositories;
using DataCollector.Core.Models;
using DataCollector.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DataCollector.Core.Services {
    public class CoinDataSaveService : ICoinDataSaveService {

        private readonly IBookTickerRepository _bookTickerRepository;
        private readonly IKlineRepository _klineRepository;
        private readonly IDayTickerRepository _dayTickerRepository;

        public CoinDataSaveService(IBookTickerRepository bookTicker, 
            IKlineRepository kline,
            IDayTickerRepository dayTicker) {

            _bookTickerRepository = bookTicker;
            _dayTickerRepository = dayTicker;
            _klineRepository = kline;
        }

        public async Task<bool> SaveData(CoinData data) {
            await _bookTickerRepository.SaveBookTicker(data.Coin, data.Exchange, data.DayTicker);
            await _dayTickerRepository.SaveDayTicker(data.Coin, data.Exchange, data.DayTicker);
            foreach (KlineModel kline in data.Klines) {
                await _klineRepository.SaveDayTicker(data.Coin, data.Exchange, kline);
            }
            return true;
        }
    }
}
