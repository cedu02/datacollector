﻿using DataCollector.Interfaces;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace DataCollector.Core.Services {
    public class HttpGetService : IHttpGetService {

        private readonly IHttpClientFactory _clientFactory;

        public HttpGetService(IHttpClientFactory clientFactory) {
            _clientFactory = clientFactory;
        }

        public async Task<HttpResponseMessage> SendGetRequest(string endpoint) {
            var request = new HttpRequestMessage(HttpMethod.Get, endpoint);
            var client = _clientFactory.CreateClient();
            HttpResponseMessage response;

            try {
                response = await client.SendAsync(request).ConfigureAwait(false);
                return response;
            } catch (Exception) {
                throw;
            } finally {
                request.Dispose();
                client.Dispose();
            }
        }
    }
}
