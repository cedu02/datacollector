﻿using DataCollector.Core.Models;
using DataCollector.UseCase.Exceptions;
using DataCollector.UseCase.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace DataCollector.UseCase.Services {
    public static class  ExchangeDataCollectorFactory {

        public static IExchangeDataCollector GetExchangeDataCollector(ExchangeModel exchange) {
            var assembly = Assembly.GetExecutingAssembly();
            var types = assembly.ExportedTypes;
            if (types == null) {
                throw new ExchangeNotImplementedException();
            }

            var type = types.Where(x => x.Name == exchange.Name + "DataCollector")
                                .FirstOrDefault();
            if (type == null || type.FullName == null) {
                throw new ExchangeNotImplementedException();
            }

            var collector = Activator.CreateInstanceFrom(assembly.Location, type.FullName)?.Unwrap();
            if (collector == null) {
                throw new ExchangeNotImplementedException();
            }

            return (IExchangeDataCollector)collector;
        }
    }
}
