﻿using DataCollector.Core.Interfaces.Repositories;
using DataCollector.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DataCollector.UseCase.Services {
    public class CoinDataCollectorService {


        private readonly IExchangeRepository _exchangeRepository;

        public CoinDataCollectorService(IExchangeRepository exchangeRepository) {
            _exchangeRepository = exchangeRepository;
        }

        public async Task<bool> CollectCoinData(CoinModel coin) {
            foreach (var exchange in await _exchangeRepository.GetExchangesOfCoin(coin)) {
                var coinResult = await ExchangeDataCollectorFactory
                                        .GetExchangeDataCollector(exchange)
                                        .CollectDataForCoin(coin);
                await CoinDataSaveService(coinResult);
            }
            return true;
        }
    }
}