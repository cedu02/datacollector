﻿using DataCollector.Core.Models;
using DataCollector.External.Database.Models;
using DataCollector.Interfaces;
using DataCollector.Services.Binance;
using DataCollector.UseCase.Interfaces;
using DataCollector.UseCase.Models;
using DataCollector.UseCase.Models.Binance;
using DataCollector.UseCase.Services.Binance.BookTicker;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DataCollector.UseCase.Services {
    public class BinanceDataCollector: IExchangeDataCollector {

        private readonly BinanceBookTickerService _bookTickerService;
        private readonly BinanceDayTickerService _dayTickerService;
        private readonly BinanceKlineService _klineService;

        public BinanceDataCollector(BinanceKlineService binanceKlineService,
            BinanceBookTickerService bookTickerService,
            BinanceDayTickerService dayTickerService) {

            _klineService = binanceKlineService;
            _dayTickerService = dayTickerService;
            _bookTickerService = bookTickerService;


        }
        public async Task<CoinData> CollectDataForCoin(CoinModel coin) {
            var result = new CoinData();
            var symbol = new BinanceSymbolOnlyParameter("BTC" + coin.Name);
            
            result.BookTicker = await _bookTickerService
                .GetBookTicker(new DayTickerEndpointBinance(symbol));

            result.DayTicker = await _dayTickerService.GetDayTicker(new DayTickerEndpointBinance(symbol));


            BinanceKlineInterval.GetAllIntervals().ForEach(async interval => {
                result.Klines.Add(await _klineService.GetKline(
                    new KlineEndpointBinance(
                        new BinanceKlineParameters(symbol.Symbol, interval))));
            });

            return result;
        }
    }
}
