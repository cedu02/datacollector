﻿using DataCollector.Interfaces;
using DataCollector.Models;
using DataCollector.UseCase.Exceptions;
using System.Net.Http;
using System.Threading.Tasks;

namespace DataCollector.UseCase.Services {

    /// <summary>
    /// Implments KlineService for Binance API. 
    /// </summary>
    public class BinanceKlineService : IKlineService {

        private readonly IHttpGetService _httpGetService;

        public BinanceKlineService(IHttpGetService httpGetService) {
            _httpGetService = httpGetService;
        }

        /// <summary>
        /// Gets Binance API data and return Kline.
        /// </summary>
        /// <param name="endpoint">Parameters for the API request</param>
        /// <returns></returns>
        public async Task<KlineModel> GetKline(IEndpoint endpoint) {
            // Get Data form Binance API
            try {
                HttpResponseMessage response = await _httpGetService.SendGetRequest(endpoint.GetEndPoint());
                return BinanceKlineResponse.GetKLine(await response.Content.ReadAsStringAsync());
            } catch {
                throw new ServiceFailureException("Binance kline api failure.");
            }
        }
    }
}
