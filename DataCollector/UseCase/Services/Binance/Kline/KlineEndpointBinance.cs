﻿using DataCollector.Interfaces;
using DataCollector.UseCase.Models.Binance;
using System.Globalization;

namespace DataCollector.UseCase.Services {

    /// <summary>
    /// Creates the KlineEndpoint URL for the Binance API with given Parameters.
    /// </summary>
    public class KlineEndpointBinance : BinanceApi, IEndpoint {

        private readonly BinanceKlineParameters _klineParameters;

        /// <summary>
        /// Creates a KlineEndpointBinance Object.
        /// </summary>
        /// <param name="klineParametersBinance">Parameters of the KlineEndpoint.</param>
        public KlineEndpointBinance(BinanceKlineParameters klineParametersBinance) {
            _klineParameters = klineParametersBinance;
        }

        /// <summary>
        /// Get the Endpoint to for the API Request.
        /// </summary>
        /// <returns>URL with parameters set from _klineParameters</returns>
        public string GetEndPoint() {
            return string.Format(CultureInfo.CurrentCulture,
                BaseApiUrl,
                "api/v3/klines?symbol={0}&interval={1}&limit=1",
                _klineParameters.Symbol,
                _klineParameters.Interval.Value);
        }
    }
}
