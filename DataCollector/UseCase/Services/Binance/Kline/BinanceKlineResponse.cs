﻿using DataCollector.Models;
using System;
using System.Globalization;
using System.Text.Json;

namespace DataCollector.UseCase.Services {

    /// <summary>
    /// Converts Binance Json Array to Kline.
    /// </summary>
    public static class BinanceKlineResponse {

        private enum ResponseOrder {
            OpenTime = 0,
            Open = 1,
            High = 2,
            Low = 3,
            Close = 4,
            Volume = 5,
            CloseTime = 6,
            QuoteAssetVolume = 7,
            NumberOfTrades = 8,
            TakerBuyBaseAssetVolume = 9,
            TakerBuyQuoteAssetVolume = 10
        }

        public static KlineModel GetKLine(string binanceResult) {
            //Serialize Response into 2d array and give use the inner array to create the klineResponseBinance object.
            var options = new JsonSerializerOptions {
                AllowTrailingCommas = true
            };

            var result = JsonSerializer.Deserialize<object[][]>(
                binanceResult,
                options)[0];

            return new KlineModel() {
                OpenTime = UnixToLocalTime(((JsonElement)result[(int)ResponseOrder.OpenTime]).GetInt64()),
                CloseTime = UnixToLocalTime(((JsonElement)result[(int)ResponseOrder.CloseTime]).GetInt64()),
                Open = decimal.Parse(((JsonElement)result[(int)ResponseOrder.Open]).GetString(), CultureInfo.InvariantCulture),
                High = decimal.Parse(((JsonElement)result[(int)ResponseOrder.High]).GetString(), CultureInfo.InvariantCulture),
                Low = decimal.Parse(((JsonElement)result[(int)ResponseOrder.Low]).GetString(), CultureInfo.InvariantCulture),
                Close = decimal.Parse(((JsonElement)result[(int)ResponseOrder.Close]).GetString(), CultureInfo.InvariantCulture),
                Volume = decimal.Parse(((JsonElement)result[(int)ResponseOrder.Volume]).GetString(), CultureInfo.InvariantCulture),
                NumberOfTrades = ((JsonElement)result[(int)ResponseOrder.NumberOfTrades]).GetInt64()
            };
        }


        private static DateTime UnixToLocalTime(long openTimeUnix) {
            return new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)
                 .AddMilliseconds(openTimeUnix).ToLocalTime();
        }
    }
}
