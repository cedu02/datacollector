﻿using DataCollector.Core.Models;
using DataCollector.UseCase.Models.Binance;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Text.Json;

namespace DataCollector.UseCase.Services {
    public static class BinanceBookTickerResponse {

        public static BookTickerModel GetBookTicker(string response) {
            var options = new JsonSerializerOptions {
                PropertyNameCaseInsensitive = true
            };

            var dayTickerBinance = JsonSerializer.Deserialize<BinanceBookTicker>(response, options);

            return new BookTickerModel() { 
                AskPrice = decimal.Parse(dayTickerBinance.AskPrice, CultureInfo.InvariantCulture),
                BidPrice = decimal.Parse(dayTickerBinance.BidPrice, CultureInfo.InvariantCulture),
                AskQty = Convert.ToInt64(decimal.Parse(dayTickerBinance.AskQty, CultureInfo.InvariantCulture)),
                BidQty = Convert.ToInt64(decimal.Parse(dayTickerBinance.BidQty, CultureInfo.InvariantCulture))
            };
        }
    }
}
