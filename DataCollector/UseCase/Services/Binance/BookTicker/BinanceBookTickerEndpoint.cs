﻿using DataCollector.Interfaces;
using DataCollector.UseCase.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace DataCollector.UseCase.Services.Binance.BookTicker {
    public class BinanceBookTickerEndpoint : BinanceApi, IEndpoint {

        private const string _endpointUrl = "api/v3/ticker/bookTicker";
        private readonly BinanceSymbolOnlyParameter _binanceSymbolOnlyParameter;

        public BinanceBookTickerEndpoint(BinanceSymbolOnlyParameter binanceSymbolOnlyParameters) {
            _binanceSymbolOnlyParameter = binanceSymbolOnlyParameters;
        }

        public string GetEndPoint() {
            return string.Format(CultureInfo.CurrentCulture,
              BaseApiUrl,
              _endpointUrl,
              "?symbol={0}",
              _binanceSymbolOnlyParameter.Symbol);
        }
    }
}
