﻿using DataCollector.Core.Models;
using DataCollector.Interfaces;
using DataCollector.UseCase.Exceptions;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace DataCollector.UseCase.Services {
    public class BinanceBookTickerService : IBookTickerService {

        private readonly IHttpGetService _httpGetService;
        
        public BinanceBookTickerService(IHttpGetService httpGetService) {
            _httpGetService = httpGetService;
        }

        public async Task<BookTickerModel> GetBookTicker(IEndpoint endpoint) {
            // Get Data form Binance API
            try {
                HttpResponseMessage response = await _httpGetService.SendGetRequest(endpoint.GetEndPoint());
                return BinanceBookTickerResponse.GetBookTicker(await response.Content.ReadAsStringAsync());
            } catch {
                throw new ServiceFailureException("Binance book ticker api failure.");
            }
        }
    }
}
