﻿using DataCollector.Interfaces;
using DataCollector.Models;
using DataCollector.UseCase.Exceptions;
using DataCollector.UseCase.Models.Binance;
using DataCollector.UseCase.Services;
using System;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;

namespace DataCollector.Services.Binance {
    /// <summary>
    /// Collect DayTicker form Binance API
    /// </summary>
    public class BinanceDayTickerService : IDayTickerService {

        private readonly IHttpGetService _httpGetService;

        public BinanceDayTickerService(IHttpGetService httpGetService) {
            _httpGetService = httpGetService;
        }

        public async Task<DayTickerModel> GetDayTicker(IEndpoint endpoint) {
            try {
                HttpResponseMessage response = await _httpGetService.SendGetRequest(endpoint.GetEndPoint());
                return DayTickerResponseBinance.GetDayTicker(await response.Content.ReadAsStringAsync());
            } catch {
                throw new ServiceFailureException("Binance dayticker api failure.");
            }
        }
    }
}
