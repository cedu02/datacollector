﻿using DataCollector.Interfaces;
using DataCollector.UseCase.Models;
using System.Globalization;

namespace DataCollector.UseCase.Services {
    public class DayTickerEndpointBinance : BinanceApi, IEndpoint {

        private const string _endpointUrl = "api/v3/ticker/24hr";
        private readonly BinanceSymbolOnlyParameter _binanceSymbolOnlyParameter;

        public DayTickerEndpointBinance(BinanceSymbolOnlyParameter binanceSymbolOnlyParameters) {
            _binanceSymbolOnlyParameter = binanceSymbolOnlyParameters;
        }

        public string GetEndPoint() {
            return string.Format(CultureInfo.CurrentCulture,
               BaseApiUrl,
               _endpointUrl, 
               "?symbol={0}",
               _binanceSymbolOnlyParameter.Symbol);
        }
    }
}
