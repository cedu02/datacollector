﻿using DataCollector.Models;
using DataCollector.UseCase.Models.Binance;
using System;
using System.Globalization;
using System.Text.Json;

namespace DataCollector.UseCase.Services{
    /// <summary>
    /// Convert BinanceDayTicker to general DayTicker
    /// </summary>
    public static class DayTickerResponseBinance {

        public static DayTickerModel GetDayTicker(string response) {
            var options = new JsonSerializerOptions {
                PropertyNameCaseInsensitive = true
            };

            var dayTickerBinance = JsonSerializer.Deserialize<BinanceDayTicker>(response, options);
            
            return new DayTickerModel {
                PriceChange = decimal.Parse(dayTickerBinance.PriceChange, CultureInfo.InvariantCulture),
                PriceChangePercent = decimal.Parse(dayTickerBinance.PriceChangePercent, CultureInfo.InvariantCulture),
                LastPrice = decimal.Parse(dayTickerBinance.LastPrice, CultureInfo.InvariantCulture),
                LastQty = decimal.Parse(dayTickerBinance.LastQty, CultureInfo.InvariantCulture),
                BidPrice = decimal.Parse(dayTickerBinance.BidPrice, CultureInfo.InvariantCulture),
                AskPrice = decimal.Parse(dayTickerBinance.AskPrice, CultureInfo.InvariantCulture),
                OpenPrice = decimal.Parse(dayTickerBinance.OpenPrice, CultureInfo.InvariantCulture),
                HighPrice = decimal.Parse(dayTickerBinance.HighPrice, CultureInfo.InvariantCulture),
                LowPrice = decimal.Parse(dayTickerBinance.LowPrice, CultureInfo.InvariantCulture),
                Volume = decimal.Parse(dayTickerBinance.Volume, CultureInfo.InvariantCulture),
                OpenTime = new DateTime(1499783499040),
                CloseTime = new DateTime(1499869899040),
                Count = 76
            };
        }
    }
}
