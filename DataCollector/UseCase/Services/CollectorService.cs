﻿using DataCollector.Core.Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataCollector.UseCase.Services {
    public class CollectorService {

        private readonly ICoinRepository _coinRepository;
        private readonly CoinDataCollectorService _coinDataCollectorService;

        public CollectorService(ICoinRepository coinRepository,
                               CoinDataCollectorService coinDataCollectorService) {
            
            _coinDataCollectorService = coinDataCollectorService;
            _coinRepository = coinRepository;
            
        }

        public async void CollectData() {
            foreach (var coin in await _coinRepository.GetActiveCoins()) {
                await _coinDataCollectorService.CollectCoinData(coin);
            }
        }
    }
}
