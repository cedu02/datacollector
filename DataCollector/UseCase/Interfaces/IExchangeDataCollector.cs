﻿using DataCollector.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DataCollector.UseCase.Interfaces {
    public interface IExchangeDataCollector {

        public Task<CoinData> CollectDataForCoin(CoinModel coin);
    }
}
