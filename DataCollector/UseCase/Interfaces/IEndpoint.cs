﻿using System;

namespace DataCollector.Interfaces {
    public interface IEndpoint {
        public String GetEndPoint();
    }
}
