﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataCollector.UseCase.Exceptions {
    public class ExchangeNotImplementedException : Exception{

        public ExchangeNotImplementedException() {
        }

        public ExchangeNotImplementedException(string message) : base(message) {
        }

        public ExchangeNotImplementedException(string message, Exception inner)
            : base(message, inner) {
        }

    }
}
