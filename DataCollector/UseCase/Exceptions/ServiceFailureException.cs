﻿using System;

namespace DataCollector.UseCase.Exceptions {
    public class ServiceFailureException : Exception {
        public ServiceFailureException() {
        }

        public ServiceFailureException(string message) : base(message) {
        }

        public ServiceFailureException(string message, Exception inner)
            : base(message, inner) {
        }
    }
}
