﻿namespace DataCollector.UseCase.Models.Binance {
    public class BinanceDayTicker {
        public string Symbol { get; set; } = string.Empty;
        public string PriceChange { get; set; } = string.Empty;
        public string PriceChangePercent { get; set; } = string.Empty;
        public string WeightedAvgPrice { get; set; } = string.Empty;
        public string PrevClosePrice { get; set; } = string.Empty;
        public string LastPrice { get; set; } = string.Empty;
        public string LastQty { get; set; } = string.Empty;
        public string BidPrice { get; set; } = string.Empty;
        public string AskPrice { get; set; } = string.Empty;
        public string OpenPrice { get; set; } = string.Empty;
        public string HighPrice { get; set; } = string.Empty;
        public string LowPrice { get; set; } = string.Empty;
        public string Volume { get; set; } = string.Empty;
        public string QuoteVolume { get; set; } = string.Empty;
        public long OpenTime { get; set; }
        public long CloseTime { get; set; }
        public long FirstId { get; set; }
        public long LastId { get; set; }
        public long Count { get; set; }
    }
}
