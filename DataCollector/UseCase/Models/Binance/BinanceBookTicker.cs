﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataCollector.UseCase.Models.Binance {
    public class BinanceBookTicker {
        public string Symbol { get; set; } = string.Empty;
        public string BidPrice { get; set; } = string.Empty;
        public string BidQty { get; set; } = string.Empty;
        public string AskPrice { get; set; } = string.Empty;
        public string AskQty { get; set; } = string.Empty;
    }
}
