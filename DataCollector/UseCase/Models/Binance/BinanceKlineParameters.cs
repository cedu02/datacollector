﻿using System.Collections.Generic;
using System.Linq;

namespace DataCollector.UseCase.Models.Binance {

    /// <summary>
    /// Kline Parameters for Binance API. 
    /// </summary>
    public class BinanceKlineParameters {

        public string Symbol { get; set; }
        public BinanceKlineInterval Interval { get; set; }

        public BinanceKlineParameters(string symbol, BinanceKlineInterval interval) {
            Symbol = symbol;
            Interval = interval;
        }
    }

    /// <summary>
    /// Time Interval Enum for all possible Intervals of the Binance API.
    /// </summary>
    public class BinanceKlineInterval {

        private BinanceKlineInterval(string value) { Value = value; }

        public string Value { get; set; }

        public static BinanceKlineInterval OneMin { get { return new BinanceKlineInterval("1m"); } }
        public static BinanceKlineInterval ThreeMin { get { return new BinanceKlineInterval("3m"); } }
        public static BinanceKlineInterval FiveMin { get { return new BinanceKlineInterval("5m"); } }
        public static BinanceKlineInterval ThirtyMin { get { return new BinanceKlineInterval("30m"); } }
        public static BinanceKlineInterval OneHour { get { return new BinanceKlineInterval("1h"); } }
        public static BinanceKlineInterval TwoHour { get { return new BinanceKlineInterval("2h"); } }
        public static BinanceKlineInterval FourHour { get { return new BinanceKlineInterval("4h"); } }
        public static BinanceKlineInterval SixHour { get { return new BinanceKlineInterval("6h"); } }
        public static BinanceKlineInterval EightHour { get { return new BinanceKlineInterval("8h"); } }
        public static BinanceKlineInterval TwelveHour { get { return new BinanceKlineInterval("12h"); } }
        public static BinanceKlineInterval OneDay { get { return new BinanceKlineInterval("1d"); } }
        public static BinanceKlineInterval ThreeDay { get { return new BinanceKlineInterval("3d"); } }
        public static BinanceKlineInterval OneWeek { get { return new BinanceKlineInterval("1w"); } }
        public static BinanceKlineInterval OneMonth { get { return new BinanceKlineInterval("1M"); } }

        public static List<BinanceKlineInterval> GetAllIntervals() {
            return new List<BinanceKlineInterval>() {
                OneMin,
                ThreeMin,
                FiveMin,
                ThirtyMin,
                OneHour,
                TwelveHour,
                OneDay,
                OneWeek,
                OneMonth
            };
        }

    }
}
}