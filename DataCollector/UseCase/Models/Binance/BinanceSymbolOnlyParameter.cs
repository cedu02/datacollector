﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataCollector.UseCase.Models {
    public class BinanceSymbolOnlyParameter {
        public string Symbol { get; set; } = string.Empty;

        public BinanceSymbolOnlyParameter(string symbol) {
            Symbol = symbol;
        }
    }
}
