﻿using DataCollector.Core.Interfaces.Repositories;
using DataCollector.Core.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataCollector.External.Database.Repositories {
    public class ExchangeRepository : IExchangeRepository {

        private readonly CoinBroContext _coinBroContext;

        public ExchangeRepository(CoinBroContext coinBroContext) {
            _coinBroContext = coinBroContext;
        }

        public async Task<List<ExchangeModel>> GetExchangesOfCoin(CoinModel coin) {
            return await _coinBroContext.Coins
                .Where(coin => coin.CoinId == coin.CoinId)
                .Select(coin => coin.ExchangeCoins.ForEach(excoin => new ExchangeModel() { 
                    ExchangeId = excoin.ExchangeId,
                    Name = excoin.
                })
                .ToListAsync();
        }
    }
}
