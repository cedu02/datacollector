﻿using DataCollector.Core.Interfaces.Repositories;
using DataCollector.Core.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataCollector.External.Database.Repositories {
    public class CoinRepository : ICoinRepository {

        private readonly CoinBroContext _coinBroContext;

        public CoinRepository(CoinBroContext coinBroContext) {
            _coinBroContext = coinBroContext;
        }

        public async Task<List<CoinModel>> GetActiveCoins() {
           return await _coinBroContext.Coins
                        .Where(coin => coin.Active)
                        .Select(coin => new CoinModel() {
                            CoinId = coin.CoinId,
                            Name = coin.Name
                        })
                        .ToListAsync();
        }
    }
}
