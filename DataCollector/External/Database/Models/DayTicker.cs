﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataCollector.External.Database.Models {
    public class DayTicker {
        public long DayTickerId { get; set; }
        public long ExchangeId { get; set; }
        public Exchange Exchange { get; set; } = null!;
        public long CoinId { get; set; }
        public Coin Coin { get; set; } = null!;
        public Decimal PriceChange { get; set; }
        public Decimal PriceChangePercent { get; set; }
        public Decimal PrevClosePrice { get; set; }
        public Decimal LastPrice { get; set; }
        public Decimal LastQty { get; set; }
        public Decimal BidPrice { get; set; }
        public Decimal AskPrice { get; set; }
        public Decimal OpenPrice { get; set; }
        public Decimal HighPrice { get; set; }
        public Decimal LowPrice { get; set; }
        public Decimal Volume { get; set; }
        public DateTime OpenTime { get; set; }
        public DateTime CloseTime { get; set; }
        public long Count { get; set; }
    }
}
