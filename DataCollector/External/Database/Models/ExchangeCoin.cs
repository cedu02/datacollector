﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataCollector.External.Database.Models {
    public class ExchangeCoin {
        public long CoinId { get; set; }
        public Coin Coin { get; set; } = null!;

        public long ExchangeId { get; set; }
        public Exchange Exchange { get; set; } = null!;
    }
}
