﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataCollector.External.Database.Models {
    public class Kline {
        public long KlineId { get; set; }
        public long ExchangeId { get; set; }
        public Exchange Exchange { get; set; } = null!;
        public long CoinId { get; set; }
        public Coin Coin { get; set; } = null!;
        public DateTime OpenTime { get; set; }
        public decimal Open { get; set; }
        public decimal High { get; set; }
        public decimal Low { get; set; }
        public decimal Close { get; set; }
        public decimal Volume { get; set; }
        public long NumberOfTrades { get; set; }
        public DateTime CloseTime { get; set; }

    }
}
