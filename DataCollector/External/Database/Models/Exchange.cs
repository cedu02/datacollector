﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DataCollector.External.Database.Models {
    public class Exchange {
        public long ExchangeId { get; set; }
        public string Name { get; set; } = string.Empty;
        public string BaseApiUrl { get; set; } = string.Empty;
        public string ExchangeUrl { get; set; } = string.Empty;

        public List<ExchangeCoin> ExchangeCoins { get; set; } = null!;
        public List<Kline> Klines { get; set; } = null!;
        public List<DayTicker> DayTickers { get; set; } = null!;
        public List<BookTicker> BookTickers { get; set; } = null!;
    }
}
