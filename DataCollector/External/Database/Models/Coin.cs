﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DataCollector.External.Database.Models {
    public class Coin {
        public long CoinId { get; set; }
        public string Name { get; set; } = string.Empty;
        public string CMCUrl { get; set; } = string.Empty;
        public bool Active { get; set; }

        public List<ExchangeCoin> ExchangeCoins { get; set; } = null!;
        public List<Kline> Klines { get; set; } = null!;
        public List<DayTicker> DayTickers { get; set; } = null!;
        public List<BookTicker> BookTickers { get; set; } = null!;
    }
}
