﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataCollector.External.Database.Models {
    public class BookTicker {
        public long BookTickerId { get; set; }
        public long ExchangeId { get; set; }
        public Exchange Exchange { get; set; } = null!;
        public long CoinId { get; set; }
        public Coin Coin { get; set; } = null!;
        public decimal BidPrice { get; set; }
        public long BidQty { get; set; }
        public decimal AskPrice { get; set; }
        public long AskQty { get; set; }

    }
}
