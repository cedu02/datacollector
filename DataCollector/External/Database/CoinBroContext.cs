﻿using DataCollector.External.Database.Models;
using Microsoft.EntityFrameworkCore;

namespace DataCollector.External.Database {

    public class CoinBroContext : DbContext {
        public DbSet<Coin> Coins { get; set; } = null!;
        public DbSet<Exchange> Exchanges { get; set; } = null!;
        public DbSet<Kline> Klines { get; set; } = null!;
        public DbSet<DayTicker> DayTickers { get; set; } = null!;
        public DbSet<BookTicker> BookTickers { get; set; } = null!;

        protected override void OnConfiguring(DbContextOptionsBuilder options)
            => options.UseSqlite("Data Source=coinbro.db");

        protected override void OnModelCreating(ModelBuilder modelBuilder) {
            modelBuilder.Entity<Coin>(coin => {

                coin.HasKey(c => c.CoinId);

                coin.Property(c => c.Name)
                    .IsRequired()
                    .HasColumnType("nvarchar(50)");

                coin.Property(c => c.CMCUrl)
                    .IsRequired()
                    .HasColumnType("nvarchar(250)");

            });

            modelBuilder.Entity<Exchange>(exchange => {

                exchange.HasKey(e => e.ExchangeId);

                exchange.Property(e => e.Name)
                        .IsRequired()
                        .HasColumnType("nvarchar(50)");

                exchange.Property(e => e.BaseApiUrl)
                        .IsRequired()
                        .HasColumnType("nvarchar(250)");

                exchange.Property(e => e.ExchangeUrl)
                        .IsRequired()
                        .HasColumnType("nvarchar(250)");
            });

            modelBuilder.Entity<ExchangeCoin>(excoin => {
                excoin.HasKey(ec => new { ec.CoinId, ec.ExchangeId });

                excoin.HasOne(ec => ec.Coin)
                      .WithMany(coin => coin.ExchangeCoins)
                      .HasForeignKey(ec => ec.CoinId);

                excoin.HasOne(ec => ec.Exchange)
                      .WithMany(exchange => exchange.ExchangeCoins)
                      .HasForeignKey(ec => ec.ExchangeId);
            });

            modelBuilder.Entity<Kline>(kline => {
                kline.HasKey(k => k.KlineId);

                kline.HasOne(k => k.Coin)
                    .WithMany(coin => coin.Klines)
                    .HasForeignKey(k => k.CoinId);

                kline.HasOne(k => k.Exchange)
                    .WithMany(exchange => exchange.Klines)
                    .HasForeignKey(k => k.ExchangeId);

            });

            modelBuilder.Entity<DayTicker>(dayticker => {
                dayticker.HasKey(k => k.DayTickerId);

                dayticker.HasOne(k => k.Coin)
                    .WithMany(coin => coin.DayTickers)
                    .HasForeignKey(k => k.CoinId);

                dayticker.HasOne(k => k.Exchange)
                    .WithMany(exchange => exchange.DayTickers)
                    .HasForeignKey(k => k.ExchangeId);

            });


            modelBuilder.Entity<BookTicker>(bookticker => {
                bookticker.HasKey(k => k.BookTickerId);

                bookticker.HasOne(k => k.Coin)
                    .WithMany(coin => coin.BookTickers)
                    .HasForeignKey(k => k.CoinId);

                bookticker.HasOne(k => k.Exchange)
                    .WithMany(exchange => exchange.BookTickers)
                    .HasForeignKey(k => k.ExchangeId);

            });
        }
    }
}
