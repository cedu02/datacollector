﻿using Xunit;
using DataCollector.Services;
using DataCollector.Interfaces;
using DataCollector.Core;
using DataCollector.Models;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Extensions.DependencyInjection;
using System.Net.Http;

namespace DataCollector.Services.Tests {
    public class KlineServiceBinanceTests {
        [Fact()]
        public async void GetKlineAsyncTest() {
           
            IServiceCollection services = new ServiceCollection();
            services.AddHttpClient();

            IHttpClientFactory factory = services
            .BuildServiceProvider()
            .GetRequiredService<IHttpClientFactory>();

            IHttpGetService httpGetService = new HttpGetService(factory);
            IKlineService klineServiceBinance = new BinanceKlineService(httpGetService);
            Kline kline = await klineServiceBinance.GetKlineAsync(new KlineEndpointBinance(
                new KlineParametersBinance("LTCBTC", Interval.oneMin)));

            Assert.Equal(kline.OpenTime.Day, DateTime.Now.Day);
            Assert.Equal(kline.OpenTime.Hour, DateTime.Now.Hour);

        }
    }
} 