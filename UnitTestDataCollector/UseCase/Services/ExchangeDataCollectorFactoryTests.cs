﻿using Xunit;
using DataCollector.UseCase.Services;
using System;
using System.Collections.Generic;
using System.Text;
using DataCollector.Core.Models;
using DataCollector.UseCase.Exceptions;

namespace DataCollector.UseCase.Services.Tests {
    public class ExchangeDataCollectorFactoryTests {
        [Fact()]
        public void GetExchangeDataCollector_ExistingExchangeClass_ShouldReturnBinanceDataCollector() {
            // Arrange 
            var exchange = new ExchangeModel() {
                Name = "Binance"
            };
            var expected = new BinanceDataCollector();
            // Act 
            var actual = ExchangeDataCollectorFactory.GetExchangeDataCollector(exchange); 
            // Assert
            Assert.Equal(expected.GetType(), actual.GetType());
        }

        [Fact()]
        public void GetExchangeDataCollector_NotExistingExchange_ShouldThrowException() {
            // Arrange 
            var exchange = new ExchangeModel() {
                Name = "NotImplemented"
            };
            var expected = new BinanceDataCollector();
            // Act 
            Action act = () => ExchangeDataCollectorFactory.GetExchangeDataCollector(exchange);
            //Assert
            Assert.Throws<ExchangeNotImplementedException>(act);
        }
    }
}