﻿using Xunit;
using DataCollector.Services.Binance;
using System;
using System.Collections.Generic;
using System.Text;
using UnitTestDataCollector.DataAttributes;
using Moq;
using DataCollector.Interfaces;
using System.Net.Http;
using System.Threading.Tasks;
using System.Net;
using DataCollector.Models;
using DataCollector.UseCase.Exceptions;

namespace DataCollector.Services.Binance.Tests {

    public class BinanceDayTickerServiceTests {

        private readonly Mock<IHttpGetService> _httpGetServiceStub = new Mock<IHttpGetService>();
        private readonly Mock<IEndpoint> _endpoint = new Mock<IEndpoint>();


        public BinanceDayTickerServiceTests() {
            _endpoint.Setup(point => point.GetEndPoint())
                .Returns(string.Empty);
        }

        private BinanceDayTickerService CreateBinanceDayTickerService() {
            return new BinanceDayTickerService(_httpGetServiceStub.Object);
        }



        private void SetupMockService(HttpResponseMessage returnValue) {
            _httpGetServiceStub
                .Setup(http => http.SendGetRequest(It.IsAny<string>()))
                .Returns(Task.FromResult(returnValue));
        }

        [Theory()]
        [JsonFileDataAttribute("Rescources/DayTicker/Binance/BinanceDayTicker.json")]
        public async void GetDayTicker_APIAccessible_ShouldReturnDayTicker(string data) {
            // Arrange 
            SetupMockService(new HttpResponseMessage {
                StatusCode = HttpStatusCode.OK,
                Content = new StringContent(data)
            });

            var expected = new DayTickerModel() {
                OpenPrice = 99.00000000m,
                AskPrice = 4.00000200m,
                LastPrice = 4.00000200m
            };
            // Act
            var actual = await CreateBinanceDayTickerService().GetDayTicker(_endpoint.Object);
            // Assert
            Assert.Equal(expected.OpenPrice, actual.OpenPrice);
            Assert.Equal(expected.AskPrice, actual.AskPrice);
            Assert.Equal(expected.LastPrice, actual.LastPrice);
        }

        [Fact]
        public async void GetDayTicker_APINotAccessible_ShouldThrowServiceFailureExceptio() {
            // Arrange 
            SetupMockService(new HttpResponseMessage {
                StatusCode = HttpStatusCode.NotFound,
                Content = new StringContent(string.Empty)
            });
            var binanceDayTickerService = CreateBinanceDayTickerService();
            // Act and Assert
            Exception ex = await Assert.ThrowsAsync<ServiceFailureException>(async () =>
            await binanceDayTickerService.GetDayTicker(_endpoint.Object));

        }
    }
}