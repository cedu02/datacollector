﻿using Xunit;
using DataCollector.UseCase.Services;
using System;
using System.Collections.Generic;
using System.Text;
using UnitTestDataCollector.DataAttributes;
using Moq;
using DataCollector.Interfaces;
using System.Net.Http;
using System.Threading.Tasks;
using System.Net;
using DataCollector.Core.Models;
using DataCollector.UseCase.Exceptions;

namespace DataCollector.UseCase.Services.Tests {
    public class BinanceBookTickerServiceTests {

        private readonly Mock<IHttpGetService> _httpGetServiceStub = new Mock<IHttpGetService>();
        private readonly Mock<IEndpoint> _endpoint = new Mock<IEndpoint>();


        public BinanceBookTickerServiceTests() {
            _endpoint.Setup(point => point.GetEndPoint())
                .Returns(string.Empty);
        }

        private BinanceBookTickerService CreateBinanceBookTickerService() {
            return new BinanceBookTickerService(_httpGetServiceStub.Object);
        }



        private void SetupMockService(HttpResponseMessage returnValue) {
            _httpGetServiceStub
                .Setup(http => http.SendGetRequest(It.IsAny<string>()))
                .Returns(Task.FromResult(returnValue));
        }


        [Theory()]
        [JsonFileDataAttribute("Rescources/BookTicker/Binance/BinanceBookTicker.json")]
        public async void GetBookTicker_APIAccessible_ShouldReturnBookTicker(string data) {
            // Arrange 
            SetupMockService(new HttpResponseMessage {
                StatusCode = HttpStatusCode.OK,
                Content = new StringContent(data)
            });

            var expected = new BookTickerModel() {
                BidPrice = 4.00000000m,
                BidQty = 431,
                AskPrice = 4.00000200m,
                AskQty = 9
            };

            // Act 
            var actual = await CreateBinanceBookTickerService().GetBookTicker(_endpoint.Object);

            // Assert
            Assert.Equal(expected.BidPrice, actual.BidPrice);
            Assert.Equal(expected.BidQty, actual.BidQty);
            Assert.Equal(expected.AskPrice, actual.AskPrice);
            Assert.Equal(expected.AskQty, actual.AskQty);
        }

        [Fact]
        public async void GetBookTicker_APINotAccessible_ShouldThrowServiceFailureExceptio() {
            // Arrange 
            SetupMockService(new HttpResponseMessage {
                StatusCode = HttpStatusCode.NotFound,
                Content = new StringContent(string.Empty)
            });
            var binanceBookTickerService = CreateBinanceBookTickerService();
            // Act and Assert
            Exception ex = await Assert.ThrowsAsync<ServiceFailureException>(async () =>
            await binanceBookTickerService.GetBookTicker(_endpoint.Object));

        }

    }
}