﻿using DataCollector.Interfaces;
using DataCollector.Models;
using DataCollector.UseCase.Exceptions;
using DataCollector.UseCase.Services;
using Moq;
using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using UnitTestDataCollector.DataAttributes;
using Xunit;

namespace DataCollector.Services.Binance.Tests {

    public class BinanceKlineServiceTests {

        private readonly Mock<IHttpGetService> _httpGetServiceStub = new Mock<IHttpGetService>();
        private readonly Mock<IEndpoint> _endpoint = new Mock<IEndpoint>();


        public BinanceKlineServiceTests() {
            _endpoint.Setup(point => point.GetEndPoint())
                .Returns(string.Empty);
        }

        private BinanceKlineService CreateBinanceKlineService() {
            return new BinanceKlineService(_httpGetServiceStub.Object);
        }

        private void SetupMockService(HttpResponseMessage returnValue) {
            _httpGetServiceStub
                .Setup(http => http.SendGetRequest(It.IsAny<string>()))
                .Returns(Task.FromResult(returnValue));
        }

        [Theory]
        [JsonFileDataAttribute("Rescources/Kline/Binance/BinanceKline.json")]
        public async void GetKline_APIAccessible_ShouldReturnKlineData(string data) {
            // Arrange 
            SetupMockService(new HttpResponseMessage {
                StatusCode = HttpStatusCode.OK,
                Content = new StringContent(data)
            });
            var binanceKlineService = CreateBinanceKlineService();

            var expected = new KlineModel() {
                Open = 0.01634790m,
                High = 0.80000000m,
                Low = 0.01575800m,
                Close = 0.01577100m,
                Volume = 148976.11427815m
            };

            // Act 
            var actual = await binanceKlineService.GetKline(_endpoint.Object);

            // Assert 
            Assert.Equal(expected.Volume, actual.Volume);
            Assert.Equal(expected.Open, actual.Open);
            Assert.Equal(expected.Low, actual.Low);
            Assert.Equal(expected.High, actual.High);
            Assert.Equal(expected.Close, actual.Close);
        }

        [Fact()]
        public async void GetKline_APINotAccessible_ShouldThrowServiceFailureException() {
            // Arrange 
            SetupMockService(new HttpResponseMessage {
                StatusCode = HttpStatusCode.NotFound,
                Content = new StringContent(string.Empty)
            });
            var binanceKlineService = CreateBinanceKlineService();
            // Act and Assert
            Exception ex = await Assert.ThrowsAsync<ServiceFailureException>(async () => await binanceKlineService.GetKline(_endpoint.Object));
        }
    }
}